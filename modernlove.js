var ghosted = false;

// clears body of messages and starts over
function reset() {
    var body = document.getElementById('middle');

    body.innerHTML = '';
    ghosted = false;
    start();
}

// displays thought bubble & fetches a message, random or pre-determined
// possibility of being ghosted is introduced
function think(random/*bool*/) {
    var bubble = document.getElementById('thinking');
    var msg = 'Hey';
    var xhr = new XMLHttpRequest();

    /* display thinking bubble */
    bubble.style.display = 'block';

    if (Math.random() > 0.5) { /* initial message */
        msg = 'U up?';
    }
    if (random) { /* random generate random responses */
        xhr.open('GET', './texts.txt', true);
        xhr.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                var options = this.responseText.split('\n');
                options.pop();
                msg = options[Math.floor(Math.random() * options.length)];
            } else if (this.readyState === 4 && this.status !== 200) {
                console.log('error fetching response');
            }
        };
        xhr.send();
    }
    /* small chance of getting ghosted */
    if (Math.random() < 0.1) {
        ghosted = true;
        console.log('ghosted!');
    }

    /* send reply msg (or not) */
    if (!ghosted) {
        setTimeout(function() { reply(msg); }, 3000);
    } else {
        setTimeout(function() { bubble.style.display = 'none'; }, 6000);
    }
}

// displays reply msg
function reply(msg) {
    var think = document.getElementById('thinking');
    var main = document.getElementById('middle');
    var nu = '<div class="row"><div class="msg received">' + msg + '</div></div>';

    think.style.display = 'none';
    main.innerHTML += nu;
    scrollDown(main);
}

// keeps window focused on most recent messages
function scrollDown(elem) {
    elem.scrollTop = elem.scrollHeight;
}

// displays typed messages
function send() {
    var box = document.getElementById('entry');
    var txt = box.value.replace(/</g, '&lt').replace(/>/g, '&gt');
    var main = document.getElementById('middle');
    var nu = '<div class="row"><div class="msg sent">' + txt + '</div></div>';

    if (txt !== '') {
        main.innerHTML += nu;
        box.value = '';
        scrollDown(main);
        /* get a response */
        if (!ghosted) {
            setTimeout(function() { think(true); }, Math.random() * 10000);
        }
    }
}

// thinks then sends 'hey' or 'u up?'
function hey() {
    think(false);
}

function start() {
    if (Math.random() > 0.5) {
        setTimeout(hey, 2000);
    }
}

start();
